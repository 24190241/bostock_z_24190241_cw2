//defining the public elements that will be called by multiple functions
readline = require('readline-sync');
var credit = 0;
var products = {};
products['Water'] = 0.8;
products['Pop'] = 1;
products['Crisps'] = 1.2;
products['Chocolate'] = 1.3;
products['Sweets'] = 0.6;

//Function to view the list of products
function view_products() {
  console.log(products);
  another_task();
}

//Function to see how much credit the customer currently has
function view_credit() {
  console.log('You currently have: ' + credit + ' pound(s)');
  another_task();
}

//Function to add credit to the customer's account
//The use will need to add credit before being able to buy any products
//The user is asked how much credit they would like to add
//The code then converts this input into a number
//Then the input is added to the credit that the user already has
function add_credit() {
  console.log('Please enter the value in the format 00.00. Do not add pound signs.');
  var addition = Number(readline.question('Enter the amount of credit you wish to add: '));
  credit = credit + addition;
  console.log('you have added ' + addition + ' pound(s)');
  console.log('You currently have: ' + credit + ' pound(s)');
  another_task();
}

//Function to purchase a product
//The user will first enter which product they would like to buy
//The code will then return the price of the product and compare it to the amount of credit the user has
//If the user has enough credit, then they can buy the product,
//if not they will be told they do not have enought credit and then asked if they would like to complete another task
function buy_product() {
  console.log('Available products:');
  console.log(products);
  var chosen_product = readline.question('Please enter the name of the product you would like to buy: ');
  if (chosen_product == 'Water') {
    price = products['Water'];
    if(price <= credit) {
      credit = (credit - price).toFixed(2);
      console.log('You have just bought a bottle of water');
      console.log('You have ' + credit + 'pound(s) left');
    } else {
      console.log('You do not have enough credit to buy this product');
    }
  } else if (chosen_product == 'Pop') {
    price = products['Pop'];
    if(price <= credit) {
      credit = (credit - price).toFixed(2);
      console.log('You have just bought a bottle of pop');
      console.log('You have ' + credit + 'pound(s) left');
    } else {
      console.log('You do not have enough credit to buy this product');
    }
  } else if (chosen_product == 'Crisps') {
    price = products['Crisps'];
    if(price <= credit) {
      credit = (credit - price).toFixed(2);
      console.log('You have just bought a packet of crisps');
      console.log('You have ' + credit + 'pound(s) left');
    } else {
      console.log('You do not have enough credit to buy this product');
    }
  } else if (chosen_product == 'Chocolate') {
    price = products['Chocolate'];
    if(price <= credit) {
      credit = (credit - price).toFixed(2);
      console.log('You have just bought a bar of chocolate');
      console.log('You have ' + credit + 'pound(s) left');
    } else {
      console.log('You do not have enough credit to buy this product');
    }
  } else if (chosen_product == 'Sweets') {
    price = products['Sweets'];
    if(price <= credit) {
      credit = (credit - price).toFixed(2);
      console.log('You have just bought a bag of sweets');
      console.log('You have ' + credit + 'pound(s) left');
    } else {
      console.log('You do not have enough credit to buy this product');
    }
  } else {
    console.log('Sorry, this is not a valid product name');
    another_task();
  }
  another_task();
}

//Function to refund credit
function refund_credit() {
  var refund = credit;
  credit = 0;
  console.log('You have been refunded ' + refund + ' pound(s)');
  console.log('Your current credit is 0');
  another_task();
}

//function for the user to select which task they would like to complete
function load() {
  console.log('1 - View Products');
  console.log('2 - View Your Credit');
  console.log('3 - Add Credit');
  console.log('4 - Buy a Product');
  console.log('5 - Refund Credit');
  var option = readline.question('Please enter the number of the task you would like to complete: ');
  if(option == 1) {
    view_products();
  } else if (option == 2) {
    view_credit();
  } else if (option == 3) {
    add_credit();
  } else if (option == 4) {
    buy_product();
  } else if (option == 5) {
    refund_credit();
  } else {
    console.log("I'm sorry, this is not a valid option. Please enter a valid option");
    load();
  }
}

//function for the user to complete multiple tasks
function another_task() {
  console.log('Would you like to comeplete another task?');
  var answer = readline.question('Please enter either "Yes" or "No": ');
  if(answer == 'Yes') {
    load();
  } else if (answer == 'No') {
    console.log('Goodbye');
  } else {
    console.log('That is not a valid option');
    another_task();
  }
}

console.log('Hello, What task would you like to complete?');
load();
